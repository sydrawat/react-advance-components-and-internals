import React, { Component } from 'react';
import Person from './Person/Person';

class Persons extends Component {
    //COMPONENT LIFE-CYCLE CREATION HOOKS
    constructor(props) {
        super(props);
        console.log('[Persons.js] constructor()', props);
    }
    componentWillMount() {
        console.log('[Persons.js] componentWillMount()');
    }
    componentDidMount() {
        console.log('[Persons.js] componentDidMount()');
    }
    //COMPONENT LIFE-CYCLE UPDATION [triggered ny parent] HOOKS
    componentWillReceiveProps(nextProps) {
        console.log('[UPDATE Persons.js] inside componentWillReceiveProps()', nextProps);
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log('[UPDATE Persons.js] inside shouldComponentUpdate()', nextProps, nextState);
        return nextProps.persons !== this.props.persons;
        //     new persons              old persons
    }
    componentWillUpdate(nextProps, nextState) {
        console.log('[UPDATE Persons.js] inside componentWillUpdate()', nextProps, nextState);
    }
    componentDidUpdate() {
        console.log('[UPDATE Persons.js] inside componentDidUpdate()');
    }
    render() {
        console.log('[Persons.js] render()');

        return (
            this.props.persons.map((person, index) => {
                return (
                    <Person
                        _onClick={this.props._onClick.bind(this, index)}
                        name={person.name}
                        age={person.age}
                        key={person.id}
                        _onChange={(event) => this.props._onChange(event, person.id)} />
                );
            })
        );
    }

}

export default Persons;