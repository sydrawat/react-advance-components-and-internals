import React, { Component } from 'react';
import classes from './Person.module.css'

class Person extends Component {
    //COMPONENT LIFE-CYCLE CREATION HOOKS
    constructor(props) {
        super(props);
        console.log('[Person.js] constructor()', props);
    }
    componentWillMount() {
        console.log('[Person.js] componentWillMount()');
    }
    componentDidMount() {
        console.log('[Person.js] componentDidMount()');
    }
    render() {
        console.log('[Person.js] render()');

        return (
            <div className={classes.Person} >
                <p onClick={this.props._onClick}>I'm {this.props.name} and I am {this.props.age} years old!</p>
                <p>{this.props.children}</p>
                <input type="text" onChange={this.props._onChange} value={this.props.name} />
            </div>
        );
    }

}

export default Person;