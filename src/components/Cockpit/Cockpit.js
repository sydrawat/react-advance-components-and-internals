import React from 'react';
import classes from './Cockpit.module.css';

const cockpit = (props) => {
    const assignedClass = [];
    let btnClass = '';

    if (props.showPersons) {
        btnClass = classes.Red;
    }

    if (props.persons.length <= 2) {
        assignedClass.push(classes.red); // cssClass = ['red']
    }
    if (props.persons.length <= 1) {
        assignedClass.push(classes.bold);// cssClass = ['red', 'bold']
    }
    return (
        <div className={classes.Cockpit}>
            <h1>{props.appTitle}</h1>
            <p className={assignedClass.join(' ')}>This is really working!</p>
            <button
                className={btnClass}
                onClick={props._onClick}>Toggle Persons</button>
        </div >
    );
};

export default cockpit;