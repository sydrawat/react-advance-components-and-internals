import React, { Component } from 'react';
import classes from './App.module.css';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';
// import ErrorBoundary from '../components/ErrorBoundary/ErrorBoundary';

class App extends Component {
  // COMPONENT CREATION LIFE-CYCLE HOOKS
  //constructor
  constructor(props) {
    super(props);
    console.log('[App.js] constructor()', props);
    //call super() before the below statement:
    this.state = {
      persons: [
        { id: 'f394h', name: 'sid', age: 24 },
        { id: 'nfo348', name: 'max', age: 28 },
        { id: '384gb', name: 'manu', age: 26 },
      ],
      otherState: 'some other value',
      showPersons: false
    };
  }

  componentWillMount() {
    console.log('[App.js] componentWillMount()');
  }

  componentDidMount() {
    console.log('[App.js] componentDidMount()');
  }
  // state = {
  //   persons: [
  //     { id: 'f394h', name: 'sid', age: 24 },
  //     { id: 'nfo348', name: 'max', age: 28 },
  //     { id: '384gb', name: 'manu', age: 26 },
  //   ],
  //   otherState: 'some other value',
  //   showPersons: false
  // }

  //COMPONENT LIFE-CYCLE UPDATION [triggered by internal changes] HOOKS
  shouldComponentUpdate(nextProps, nextState) {
    console.log('[UPDATE App.js] inside shouldComponentUpdate()', nextProps, nextState);
    return true;
  }
  componentWillUpdate(nextProps, nextState) {
    console.log('[UPDATE App.js] inside componentWillUpdate()', nextProps, nextState);
  }
  componentDidUpdate() {
    console.log('[UPDATE App.js] inside componentDidUpdate()');
  }
  deletePersonHandler = (personIndex) => {
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({
      persons: persons
    });
  }

  nameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex((prsn) => {
      return prsn.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
    };

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({ persons: persons })
  }

  togglePersonsHandler = (event) => {
    const doesShow = this.state.showPersons;
    this.setState({
      showPersons: !doesShow
    });
  }
  render() {
    console.log('[App.js] render()');

    let persons = null;

    if (this.state.showPersons) {

      persons = (
        <Persons
          persons={this.state.persons}
          _onClick={this.deletePersonHandler}
          _onChange={this.nameChangeHandler} />
      );
    }

    return (
      <div className={classes.App}>
        <Cockpit
          appTitle={this.props.title}
          showPersons={this.state.showPersons}
          persons={this.state.persons}
          _onClick={this.togglePersonsHandler} />
        {persons}
      </div>
    );
  }
}

export default App;
